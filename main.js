const inputs =document.getElementsByClassName("butt");
const iconsEye = document.querySelectorAll(".icon-password");

function passwordShow(event){
    const input = event.target.previousElementSibling;
    if(input.type === "password"){
        input.type = "text";
        event.target.classList.remove("fa-eye");
        event.target.classList.add("fa-eye-slash");
    }else{
        input.type = "password";
        event.target.classList.remove("fa-eye-slash");
        event.target.classList.add("fa-eye");
        
    }
}
iconsEye.forEach(element => {
    element.addEventListener("click",passwordShow);
});

function checkPasswords(){
   const password1 = inputs[0].value;
   const password2 = inputs[1].value
   const error = document.createElement("span");
   if(password1 === password2){
    alert("You are welcome");
   }else{
    error.innerText = "Потрібно ввести однакові значення";
     const el = inputs[1].parentNode;
     error.style.color='red';
    el.append(error);
    
   }
   
}

const button = document.querySelector('.btn');
button.addEventListener("click",checkPasswords);
